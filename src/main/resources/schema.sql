create table if not exists Stock(id int NOT NULL AUTO_INCREMENT, dtime datetime default current_timestamp on update current_timestamp, stockTicker varchar(255), price double, volume int, buyOrSell varchar(255), statusCode int DEFAULT 0, PRIMARY KEY (id));


insert into Stock(stockTicker, price, volume, buyOrSell, statusCode) values('AAPL', 1000.1, 3, 'BUY', 0);
insert into Stock(stockTicker, price, volume, buyOrSell, statusCode) values('GOOG', 520, 2, 'SELL', 0);
insert into Stock(stockTicker, price, volume, buyOrSell, statusCode) values('TCS', 670, 7, 'BUY', 0);
insert into Stock(stockTicker, price, volume, buyOrSell, statusCode) values('C', 700, 3, 'BUY', 0);
insert into Stock(stockTicker, price, volume, buyOrSell, statusCode) values('INFY', 800, 6, 'BUY', 0);